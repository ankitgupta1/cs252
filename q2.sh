#!/bin/bash

awk '
BEGIN {	FS=", ";}
{
	print "define(NAME_OF_USER," $1 ")dnl" > "all_defines";
	print "define(EMAIL_OF_USER," $2 ")dnl" >> "all_defines";
	print "define(PAN_OF_USER," $3 ")dnl" >> "all_defines";
	print "define(DONATION_OF_USER," $4 ")dnl" >> "all_defines";
	system("m4 all_defines mail-template.m4 | msmtp -t " $2);
	close("all_defines");
}' ass4.csv
