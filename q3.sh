#!/bin/bash

data_file=$1;
dest_address=$2;

printf "HELLO smtp.cse.iitk.ac.in\n" > final_mail.txt;
printf "MAIL FROM:<ankgupta@cse.iitk.ac.in>\n" >> final_mail.txt;
printf "RCPT TO:<%s>\n" "$dest_address" >> final_mail.txt;
printf "DATA\n" >> final_mail.txt;
printf "From: Ankit Gupta <ankgupta@cse.iitk.ac.in>\n" >> final_mail.txt;
printf "To:<%s>\n" "$dest_address" >> final_mail.txt;
printf "Date: $(date)\n" >> final_mail.txt;
printf "Suject: $3\n" >> final_mail.txt;
cat $data_file >> final_mail.txt;
cat final_mail.txt | nc smtp.cse.iitk.ac.in 25;
